**Journal de bord**




* **Jeudi 30 janvier** 

W + R

Visite chez WIRES et briefing sur leurs attentes et leurs exigences
Présentation rapide du code déjà disponible sur github





* **Lundi 3 février** 

W + R

Première version du cahier des charges, exigences fonctionnelles et non fonctionnelles
Installation des outils: Android Studio
Premiers pas sur Android Studio





* **Lundi 11 février** 

W + R

Récupération Git du projet
Première exécution de l’application
Compréhension du code fournis





* **Vacances** 

R

Premier essai création Timer
Premier essai création Search Bar
Décision de ne pas utiliser une search bar mais de diviser les cages en fonctions de leurs états 




* **Lundi 2 mars** 

R

Partie graphique Timer + class
Tuto implémentation Bottom navigation sur android studio





* **Mardi 3 mars** 

R

Première implémentation d’un menu pour switch entre les différentes cages





* **Lundi 9 mars** 

W+R

Pré-soutenance




* **Mardi 10 mars** 

R

Distinctions cages ouvertes, cages fermées, dispositifs non déployés (différentes classes)
Tuto implémentation système de notifications visuelles et sonores





* **Lundi 16 mars** 

W+R

Visio : tester application avec mosquito





* **Jeudi 19 mars** 

W+R

Visio avec M. Delbart : priorité → possibilité de pouvoir rentrer des informations sur les cages, rajouter une cage, changer état… + Notifications





* **vendredi 20 mars** 

W

Revue complète du code en apportant énormément de modifications afin d’améliorer les performances
Ajout de la possibilité à l’utilisateur de modifier les informations d’une cage et d’afficher en priorité ce changement




* **samedi 21 mars** 

W

Continuation d’amélioration du code existant
Ajout de la possibilité d’ajouter les “régions” où sont situées les cages et l’afficher



* **dimanche 22 mars** 

W

Mise en commun de toutes les modifications apportées et faire en sorte que tout fonctionne correctement puis commit du code



* **lundi 23 mars** 

W

Ajout de la possibilité d’entrer les coordonnées d’une cage posée selon les coordonnées actuelles de la personne qui pose la cage (fonctionnalité déjà existante mais ne fonctionnait pas donc il fallait revoir intégralement cette fonctionnalité et l’intégrer au nouveau code)
Travail en cours : afficher la position de la cage avec GoogleMaps



* **mardi 24 mars** 

R

Autre version timer premier test (réussi) avec boutons start/pause
Deuxième test sans bouton mais timer s'enclenche automatiquement au lancement de l'application et on ne peut pas le stopper --> problème à régler.
Recherches + tuto --> Comment modifier les données du serveur (REST API), methode PUT



*  **mercredi 25 mars**

W

Modifications nécessaires pour pouvoir modifier l'état d'une cage sur le server mais en attente d'identifiants pour avoir l'authorisation.



* **vendredi 27 mars**

W

Possibilité de changer les données meta d'une cage.
Cependant nous avons eu un problème lors d'une manipulation et nous attendons désormais que l'entreprise règle ce problème sur le server.



*  **lundi 30 mars**

W

Le problème a été réglé, code rectifié afin que ce problème n'arrive plus.
Début de code pour pouvoir visualiser sur Google Maps.


*  **Jeudi 02 avril**

R

Utilisation de fragments au lieu d'activities et problème du changement d'état réglé (une cage s'affichait dans opened et closed à la fois).
Timer (countdowntimer) OK).


*  **lundi 20 avril**

W

Mise en place d'une autre interface graphique avec fond noir et moins de couleurs :
- permet l'économie de batterie
- peut être plus facile de voir les informations dans certaines circonstances
- c'est plus joli



*  **dimance 26 avril**

W

Modification des fichiers .xml afin que l'affichage s'adapte à tout type d'écran sans problème.



*  **lundi 27 avril**

W

Finalisation de l'affichage d'économie de batterie (fond noir)
Il est maintenant possible de visualiser avec Google Maps.



*  **mercredi 29 avril**

W

Possibilité de retourner à l'écran d'accueil depuis n'importe où en cliquant sur le logo Wyres dans la Toolbar
Bouton refresh dans la Toolbar d'accueil pour actualiser la liste des cages
Modifications sur les fichiers .xml afin de pleinement profiter des fonctionnalités d'androidx qui sont plus complètes
Modification du design des boutons
Mise en commentaire de la possibilité de entrer une "region" de la cage
Possibilité de poster les coordonnées de la cage sur le server, cependant cela ne marche pas, cela doit venir d'un problème d'authorisation

R

Mise en place d'un système de notifications. Premier essai avec FireBase mais échec. Utilisation d'une classe Notifications qui extend de Service.
Notifications timer + Notifications batterie faible du téléphone + Notifications changement d'état

*  **jeudi 30 avril**

W

Derniers changements pour merge
Aide à Rim pour faire fonctionner le timer correctement à l'aide de threads
Merge